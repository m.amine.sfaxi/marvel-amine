import axios from 'axios';
import crypto from 'crypto';

const url = 'http://gateway.marvel.com:80/v1/public/characters';
const timestamp = [Math.round(+new Date() / 1000)];
const privateApi = '2534cef6a4d39cf43f5273da62f6d9f6e2f82bad';
const publicApi = 'b8cc9e099ccae1a54d79cb6c7767ff75';
const concatenatedString = timestamp.concat(privateApi, publicApi).join('');
const hash = crypto.createHash('md5').update(`${concatenatedString}`).digest('hex');

export const fetchCharacters = () =>
  axios.get(url, {
    params: {
      ts: timestamp,
      apikey: publicApi,
      hash,
    },
  });

export const fetchCharacterInfo = id =>
  axios.get(`${url}/${id}`, {
    params: {
      ts: timestamp,
      apikey: publicApi,
      hash,
    },
  });
