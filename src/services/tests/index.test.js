import axiosMock from 'axios';
import { fetchCharacters, fetchCharacterInfo } from '../characterService';

jest.mock('axios', () => ({
  get: jest.fn(),
}));

describe('fetchCharacters', () => {
  axiosMock.get.mockResolvedValueOnce({ data: { data: { results: [] } } });
  it('should return empty array', () => {
    return fetchCharacters().then(res => {
      const characters = res.data.data.results;
      expect(characters.length).toEqual(0);
    });
  });
});

describe('fetchCharacterInfo', () => {
  axiosMock.get.mockResolvedValueOnce({ data: { data: { results: [] } } });
  it('should return empty array', () => {
    return fetchCharacterInfo().then(res => {
      const characters = res.data.data.results;
      expect(characters.length).toEqual(0);
    });
  });
});
