import { useDispatch, useSelector } from 'react-redux';
import { getLoading, getCharacters, getCharacterInfo } from '../../selectors/marvel';
import { loadCharacters, loadCharacterInfo } from '../../ducks/marvel';

export const useFetch = () => {
  const dispatch = useDispatch();
  return {
    loadCharacters: () => dispatch(loadCharacters()),
    characters: useSelector(getCharacters),

    loadCharacterInfo: id => dispatch(loadCharacterInfo(id)),
    characterInfo: useSelector(getCharacterInfo),

    loading: useSelector(getLoading),
  };
};
