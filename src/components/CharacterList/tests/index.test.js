import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import configureStore from '../../../store';
import { Provider } from 'react-redux';

describe('components | CharacterList | index', () => {
  it('should render component', () => {
    const store = configureStore();
    const { container } = render(
      <Provider store={store}>
        <Component />
      </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});
