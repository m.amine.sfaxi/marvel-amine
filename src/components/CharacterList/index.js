import React, { useEffect } from 'react';
import Character from '../Character';
import { Grid } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import { useFetch } from './hook';
import Spinner from '../Spinner/index';

const CharacterList = () => {
  const { loadCharacters, characters, loading } = useFetch();

  useEffect(() => {
    loadCharacters();
  }, []);

  if (loading) return <Spinner />;
  return (
    <Grid container justify="center" alignItems="center" style={{ height: '100vh', margin: '20px 0' }}>
      <Grid item>
        <Typography variant="h4" style={{ padding: 26 }}>
          List of heros
        </Typography>
      </Grid>
      <Grid item container justify="space-around" alignItems="center">
        {characters.map(character => {
          return (
            <Character
              key={character.id}
              id={character.id}
              name={character.name}
              urls={character.urls}
              thumbnail={character.thumbnail}
            />
          );
        })}
      </Grid>
    </Grid>
  );
};
export default CharacterList;
