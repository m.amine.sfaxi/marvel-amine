import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import configureStore from '../../../store';
import { Provider } from 'react-redux';
import { Route, MemoryRouter } from 'react-router-dom';

describe('components | CharacterInfo | index', () => {
  it('should render component', () => {
    const store = configureStore();
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/1']}>
          <Route path="/:charachterId">
            <Component />
          </Route>
        </MemoryRouter>
      </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});
