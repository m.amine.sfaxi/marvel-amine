import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { useFetch } from '../CharacterList/hook';
import { Divider, Grid, Paper } from '@material-ui/core';
import { Typography } from '@material-ui/core';
import { Link, useParams } from 'react-router-dom';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import Spinner from '../Spinner/index';

const useStyles = makeStyles({
  root: {
    margin: 28,
  },
  link: {
    color: '#444',
    padding: 6,
    display: 'block',
  },
  image: {
    width: 280,
    height: 350,
    margin: 12,
  },
  content: {
    padding: '12px 28px',
  },
});

export default function CharacterInfo() {
  const classes = useStyles();
  let { characterId } = useParams();
  const { loadCharacterInfo, characterInfo, loading } = useFetch();

  useEffect(() => {
    loadCharacterInfo(characterId);
  }, []);
  if (loading) return <Spinner />;
  if (characterInfo.length === 0 && !loading)
    return <Typography variant="h6">404 problem loading character</Typography>;

  const imageUrl = characterInfo.thumbnail.path + '.' + characterInfo.thumbnail.extension;
  const comics = characterInfo.comics.items;
  const series = characterInfo.series.items;
  const name = characterInfo.name;
  const description = characterInfo?.description ? characterInfo.description : 'No Description for this hero ';

  return (
    <Grid container justify="center" className={classes.root}>
      <Grid item className={classes.image}>
        <Link
          to="/"
          className={classes.link}
          style={{
            padding: '18px 21px',
            background: '#333',
            color: '#fff',
            textAlign: 'center',
            borderRadius: '4px',
            textDecoration: 'none',
          }}
        >
          <Grid container justify="center" alignItems="center">
            <ArrowBackIosIcon />
            Go back home
          </Grid>
        </Link>
        <img src={imageUrl} width="280px" height="350px" />
      </Grid>
      <Grid item sm={8} className={classes.content}>
        <Paper className={classes.content}>
          <Typography variant="h4" style={{ textAlign: 'center', padding: '12px' }}>
            {name}
          </Typography>
          <Typography variant="h6">Description</Typography>
          <Typography variant="caption">{description}</Typography>
        </Paper>
        <Divider />
        <Paper className={classes.content}>
          <Typography variant="h5"> Comics </Typography>
          {comics.length ? (
            comics.map((comic, index) => {
              return (
                <Link key={index} to={comic.resourceURI} className={classes.link}>
                  {comic.name}
                </Link>
              );
            })
          ) : (
            <Typography variant="caption">no comics found</Typography>
          )}
        </Paper>
        <Divider />
        <Paper className={classes.content}>
          <Typography variant="h5"> Series </Typography>
          {series.length ? (
            series.map((serie, index) => {
              return (
                <Link key={index} to={serie.resourceURI} className={classes.link}>
                  {serie.name}
                </Link>
              );
            })
          ) : (
            <Typography variant="caption">no series found</Typography>
          )}
        </Paper>
      </Grid>
    </Grid>
  );
}
