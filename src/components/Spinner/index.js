import { Grid, CircularProgress } from '@material-ui/core';
import React from 'react';

export default function Spinner() {
  return (
    <Grid container justify="center" alignItems="center" style={{ height: '100vh' }}>
      <CircularProgress />
    </Grid>
  );
}
