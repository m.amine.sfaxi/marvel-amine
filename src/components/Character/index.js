import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Link from '@material-ui/core/Link';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
  root: {
    width: 350,
    margin: 12,
  },
  media: {
    height: 180,
  },
});

const Character = ({ name, urls, thumbnail, id }) => {
  const classes = useStyles();
  const imageUrl = thumbnail.path + '.' + thumbnail.extension;
  const history = useHistory();

  return (
    <Card className={classes.root}>
      <CardActionArea onClick={() => history.push('/' + id)}>
        <CardMedia className={classes.media} image={imageUrl} title={name} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {name}
          </Typography>
        </CardContent>
      </CardActionArea>
      <CardActions>
        {urls.map((url, index) => {
          return (
            <Link key={index} href={url.url}>
              {url.type}
            </Link>
          );
        })}
      </CardActions>
    </Card>
  );
};
Character.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  thumbnail: PropTypes.object.isRequired,
  urls: PropTypes.array.isRequired,
};
export default Character;
