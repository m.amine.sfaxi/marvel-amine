import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';

describe('components | Character | index', () => {
  it('should render component', () => {
    const props = {
      name: 'test',
      urls: [
        {
          type: 'detail',
          url:
            'http://marvel.com/characters/74/3-d_man?utm_campaign=apiRef&utm_source=b8cc9e099ccae1a54d79cb6c7767ff75',
        },
      ],
      thumbnail: { path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/e0/535fecbbb9784', extension: 'jpg' },
      id: 1111,
    };
    const { container } = render(<Component {...props} />);
    expect(container).toMatchSnapshot();
  });
});
