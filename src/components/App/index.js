import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import CharacterInfo from '../CharacterInfo';
import CharacterList from '../CharacterList';

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <CharacterList />
        </Route>
        <Route exact path="/:characterId">
          <CharacterInfo />
        </Route>
        <Route render={() => '404'}></Route>
      </Switch>
    </Router>
  );
};

export default App;
