import { fetchCharacterInfo, fetchCharacters } from '../services/characterService';

export const LOAD_CHARACTERS = 'LOAD_CHARACTERS';
export const LOAD_CHARACTER_INFO = 'LOAD_CHARACTER_INFO';
export const LOADING = 'LOADING';

export const loadCharacters = () => async dispatch => {
  dispatch({
    type: LOADING,
    loading: true,
  });
  const res = await fetchCharacters();
  const characters = res.data.data.results;
  dispatch({
    type: LOAD_CHARACTERS,
    characters,
  });
  dispatch({
    type: LOADING,
    loading: false,
  });
};

export const loadCharacterInfo = id => async dispatch => {
  dispatch({
    type: LOADING,
    loading: true,
  });
  try {
    const res = await fetchCharacterInfo(id);
    const characterInfo = res.data.data.results[0];
    dispatch({
      type: LOAD_CHARACTER_INFO,
      info: characterInfo,
    });
    dispatch({
      type: LOADING,
      loading: false,
    });
  } catch (e) {
    dispatch({
      type: LOADING,
      loading: false,
    });
  }
};

export const actions = { loadCharacters, loadCharacterInfo };

const initState = {
  characters: [],
  info: [],
  loading: true,
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case LOAD_CHARACTERS:
      return {
        ...state,
        characters: action.characters,
      };
    case LOAD_CHARACTER_INFO:
      return {
        ...state,
        info: action.info,
      };
    case LOADING:
      return { ...state, loading: action.loading };
    default:
      return state;
  }
};

export default { actions, reducer };
