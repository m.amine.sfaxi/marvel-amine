import reducers from '../../reducers';
import actions from '../../actions';
import axiosMock from 'axios';
import { configureStore } from '../../middleware';
import { LOAD_CHARACTERS, LOAD_CHARACTER_INFO } from '../marvel';
import { getCharacters, getCharacterInfo } from '../../selectors/marvel';

jest.mock('axios', () => ({
  get: jest.fn(),
}));

describe('Ducks | Marvel | loadCharacters ', () => {
  const data = { data: { results: [{}] } };
  axiosMock.get.mockResolvedValue({ data: data });

  it('should load characters', async () => {
    const initState = {
      marvel: {
        characters: [],
      },
    };
    const hook = {
      [LOAD_CHARACTERS]: getState => {
        const characters = getCharacters(getState());
        expect(characters.length).toEqual(1);
      },
    };
    const store = configureStore(reducers(), initState, hook);
    store.dispatch(actions.marvel.loadCharacters());
  });
});

describe('Ducks | Marvel | getCharacterInfo  ', () => {
  const data = { data: { results: [{}] } };
  axiosMock.get.mockResolvedValue({ data: data });
  it('should load character info', async () => {
    const initState = {
      marvel: {
        info: {},
      },
    };
    const hook = {
      [LOAD_CHARACTER_INFO]: getState => {
        const characterInfo = getCharacterInfo(getState());
        expect(characterInfo).toEqual({});
      },
    };
    const store = configureStore(reducers(), initState, hook);
    store.dispatch(actions.marvel.loadCharacterInfo(1));
  });
});
