import { prop } from 'ramda';
import { createSelector } from 'reselect';
const root = prop('marvel');

export const getCharacters = createSelector(root, prop('characters'));
export const getCharacterInfo = createSelector(root, prop('info'));
export const getLoading = createSelector(root, prop('loading'));
